<?php
namespace Inmovsoftware\PositionApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\PositionApi\Models\V1\Position;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PositionController extends Controller
{
    public function index(Request $request)
    {

    }



    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        return response()->json($user);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->validate([
            "name" => "nullable|max:255",
            "last_name" => "nullable|max:255",
            "avatar" => "nullable|max:100|unique:customers,document,".$customer->id,
            "birthday" => "required|max:100|min:10|phone:CO|unique:customers,mobile,".$customer->id,
            "password" => "nullable|integer"
        ]);
        $data["document"]=preg_replace("/[^0-9]+/", "", strtoupper($data["document"]));



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $item = $user->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }

}
